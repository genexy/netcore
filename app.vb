Imports System.IO
Imports Microsoft.AspNetCore.Hosting
Imports Microsoft.Extensions.Hosting
Namespace coret
    Public Class app
        Public Shared Sub Main(args As String())
            CreateHostBuilder(args).Build().Run()
        End Sub

        Public Shared Function CreateHostBuilder(args As String()) As IHostBuilder
            Dim builder As IHostBuilder = Host.CreateDefaultBuilder(args).UseContentRoot(Directory.GetCurrentDirectory())

            Return builder.ConfigureWebHostDefaults(Sub(webBuilder As IWebHostBuilder)
                                                        webBuilder.UseStartup(Of main)
                                                    End Sub)
        End Function
    End Class
End Namespace