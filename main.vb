Imports System.IO
Imports System.Net.WebSockets
Imports Microsoft.AspNetCore.Builder
Imports Microsoft.AspNetCore.Hosting
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Routing
Imports Microsoft.Extensions.DependencyInjection
Imports Microsoft.Extensions.FileProviders
Imports Microsoft.Extensions.Hosting
Namespace coret
    Public Class main
        Public Sub ConfigureServices(services As IServiceCollection)

        End Sub
        Public Sub Configure(app As IApplicationBuilder, env As IWebHostEnvironment)
            If env.IsDevelopment() Then
                app.UseDeveloperExceptionPage()
            End If
            Dim gWebIndexPath As String = Path.Combine(Directory.GetCurrentDirectory, "gindex") '系统工作主目录
            If Directory.Exists(gWebIndexPath) = False Then
                Directory.CreateDirectory(gWebIndexPath)
            End If
            'WebSocket模块
            app.UseWebSockets(New WebSocketOptions() With {.KeepAliveInterval = TimeSpan.FromSeconds(120)})
            app.Use(Async Function(context As HttpContext, [next] As Func(Of Task)) As Task
                        Dim sp As String() = context.Request.Path.Value.Split("/")
                        If context.WebSockets.IsWebSocketRequest And sp(1) = "ws" And UBound(sp) < 3 Then
                            Using app.ApplicationServices.CreateScope()
                                Dim ws As WebSocket = Await context.WebSockets.AcceptWebSocketAsync()
                                Await gWebSocket.wsapi.SocketHandler(context, ws)
                            End Using
                        Else
                            Await [next]()
                        End If
                    End Function)
            '首页初始化
            app.Use(Async Function(context As HttpContext, [next] As Func(Of Task)) As Task
                        Await gMiddleware.gWebIndex(context, [next], gWebIndexPath)
                    End Function)
            'HTTP错误模式
            app.UseMiddleware(Of httperr)
            '开启静态文件
            app.UseStaticFiles(New StaticFileOptions() With {.FileProvider = New PhysicalFileProvider(Path.Combine(gWebIndexPath)), .RequestPath = New PathString("")})
            '开启目录浏览
            'app.UseDirectoryBrowser(New DirectoryBrowserOptions() With {.FileProvider = New PhysicalFileProvider(Path.Combine(gWebIndexPath)), .RequestPath = New PathString("/dir")})
            '路由模式
            app.UseRouting()
            '路由模式设置
            app.UseEndpoints(Sub(endpoints As IEndpointRouteBuilder)
                                 gMiddleware.gWebTest(endpoints, "/genexy", "text/html; charset=utf-8") '测试页面
                             End Sub)
        End Sub
    End Class
End Namespace